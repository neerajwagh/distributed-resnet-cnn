import torch
import numpy as np
import time
import torchvision
import collections

torch.manual_seed(42)

# https://www.cs.toronto.edu/~kriz/cifar.html
def load_dataset(batch_size=256):
    
    # TODO: enable transforms for final training
    train_transforms = torchvision.transforms.Compose([
        torchvision.transforms.RandomCrop(32, padding=4),
        torchvision.transforms.RandomHorizontalFlip(),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean=(0.4914, 0.4822, 0.4465), std=(0.2023, 0.1994, 0.2010)),
    ])

    test_transforms = torchvision.transforms.Compose([
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean=(0.4914, 0.4822, 0.4465), std=(0.2023, 0.1994, 0.2010)),
    ])
    
    train_set = torchvision.datasets.CIFAR100(root="./cifar100-torchvision",
                                             train=True,
                                             transform=train_transforms,
                                             download=True)
    train_loader = torch.utils.data.DataLoader(train_set,
                                               batch_size=batch_size,
                                               shuffle=True,
                                               num_workers=4,
                                               pin_memory=True
                                            )

    test_set = torchvision.datasets.CIFAR100(root="./cifar100-torchvision",
                                             train=False,
                                             transform=test_transforms,
                                             download=True)
    test_loader = torch.utils.data.DataLoader(test_set,
                                               batch_size=batch_size,
                                               shuffle=False,
                                               num_workers=4,
                                               pin_memory=True
                                            )
    
    print ("** Train and Test loaders setup with batch size =", batch_size)
    return (train_loader, test_loader)


class BasicBlock(torch.nn.Module):
    def __init__(self, in_channels, out_channels, padding_size=None, kernel_size=3, stride=1):
        super(BasicBlock, self).__init__()
        self.resnet_block = torch.nn.Sequential(collections.OrderedDict([
            ("conv2d", torch.nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=1, bias=False)),
            ("batchnorm", torch.nn.BatchNorm2d(out_channels)),
            ("relu", torch.nn.ReLU(inplace=True)),
            # TODO: stride = 1 always??
            ("conv2d", torch.nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=kernel_size, stride=1, padding=1, bias=False)),
            ("batchnorm", torch.nn.BatchNorm2d(out_channels))
        ]))
        self.padding_size = padding_size
        
    def forward(self, x_minibatch):
        x = self.resnet_block(x_minibatch)
        # print("--- basic block x_minibatch size: ", x_minibatch.size())        
        # print("--- basic block output size: ", x.size())
        if (x.size() != x_minibatch.size()):
            # https://pytorch.org/docs/stable/nn.functional.html#torch.nn.functional.pad
            # TODO: pad with 1 or 0 ??
            x = torch.nn.functional.pad(x, (self.padding_size, self.padding_size, self.padding_size, self.padding_size), 'constant', 0)
            # print("x padded size: ", x.size())

        # TODO: upsample x or downsample x_minibatch ??
        x = x + x_minibatch
        outputs = torch.nn.functional.relu(x)
        return(outputs)

# Generic ResNet model specification
class ResNet_model(torch.nn.Module):

    def __init__(self, input_dim, color_depth, output_dim, hyperparams={"hidden_units":100}):
        # call the __init__() of torch.nn.Module class
        super(ResNet_model, self).__init__()
        self.input_dim = input_dim
        self.color_depth = color_depth
        self.output_dim = output_dim
        self.filter_size = hyperparams["filter_size"]

        # creating containers for each conceptual block of NN layers
        self.conv2d_block_1 = torch.nn.Sequential(collections.OrderedDict([
            ("conv2d", torch.nn.Conv2d(in_channels=self.color_depth, out_channels=32, kernel_size=self.filter_size, stride=1, padding=1, bias=False)),
            ("batchnorm", torch.nn.BatchNorm2d(32)),
            ("relu", torch.nn.ReLU(inplace=True)),
		    ("dropout", torch.nn.Dropout2d(p=0.5))
        ]))
        
        self.conv2d_block_2 = torch.nn.Sequential(collections.OrderedDict([
            ("resnet_block", BasicBlock(in_channels=32, out_channels=32, padding_size=None, kernel_size=self.filter_size, stride=1)),
            ("resnet_block_2", BasicBlock(in_channels=32, out_channels=32, padding_size=None, kernel_size=self.filter_size, stride=1)),
        ]))        
        self.conv1d_block_2 = torch.nn.Conv2d(in_channels=32, out_channels=64, kernel_size=1, bias=False, stride=1, padding=1)
        
        self.conv2d_block_3 = torch.nn.Sequential(collections.OrderedDict([
            # TODO: stride = 2 for each resnet block??
            ("resnet_block", BasicBlock(in_channels=64, out_channels=64, padding_size=8, kernel_size=self.filter_size, stride=2)),
            ("resnet_block_2", BasicBlock(in_channels=64, out_channels=64, padding_size=8, kernel_size=self.filter_size, stride=1)),
            ("resnet_block_3", BasicBlock(in_channels=64, out_channels=64, padding_size=8, kernel_size=self.filter_size, stride=1)),
            ("resnet_block_4", BasicBlock(in_channels=64, out_channels=64, padding_size=8, kernel_size=self.filter_size, stride=1)),
        ]))
        self.conv1d_block_3 = torch.nn.Conv2d(in_channels=64, out_channels=128, kernel_size=1, bias=False, stride=2, padding=1)

        self.conv2d_block_4 = torch.nn.Sequential(collections.OrderedDict([
            ("resnet_block", BasicBlock(in_channels=128, out_channels=128, padding_size=4, kernel_size=self.filter_size, stride=2)),
            ("resnet_block_2", BasicBlock(in_channels=128, out_channels=128, padding_size=4, kernel_size=self.filter_size, stride=1)),
            ("resnet_block_3", BasicBlock(in_channels=128, out_channels=128, padding_size=4, kernel_size=self.filter_size, stride=1)),
            ("resnet_block_4", BasicBlock(in_channels=128, out_channels=128, padding_size=4, kernel_size=self.filter_size, stride=1)),
        ]))
        self.conv1d_block_4 = torch.nn.Conv2d(in_channels=128, out_channels=256, kernel_size=1, bias=False, stride=2, padding=1)
        
        self.conv2d_block_5 = torch.nn.Sequential(collections.OrderedDict([
            ("resnet_block", BasicBlock(in_channels=256, out_channels=256, padding_size=2, kernel_size=self.filter_size, stride=2)),
            ("resnet_block", BasicBlock(in_channels=256, out_channels=256, padding_size=2, kernel_size=self.filter_size, stride=1)),
            # TODO: average pool used in paper
            ("pool", torch.nn.MaxPool2d(kernel_size=2, stride=2))
        ]))

        self.fc_block = torch.nn.Sequential(collections.OrderedDict([
            # TODO: direct to 100?
            ("fc_1", torch.nn.Linear(in_features=4096, out_features=self.output_dim, bias=True)),
            ("fc_1", torch.nn.Linear(in_features=4096, out_features=1000, bias=True)),
            ("relu_1", torch.nn.ReLU(inplace=True)),
            ("linear", torch.nn.Linear(in_features=1000, out_features=self.output_dim, bias=True))
        ]))
        
        # TODO: Xavier/Glorot initialization, change gain ??
        self.conv2d_block_1.apply(lambda x: torch.nn.init.xavier_normal_(x.weight, gain=1) if type(x) == torch.nn.Conv2d else None)
        self.conv2d_block_2.apply(lambda x: torch.nn.init.xavier_normal_(x.weight, gain=1) if type(x) == torch.nn.Conv2d else None)
        self.conv2d_block_3.apply(lambda x: torch.nn.init.xavier_normal_(x.weight, gain=1) if type(x) == torch.nn.Conv2d else None)
        self.conv2d_block_4.apply(lambda x: torch.nn.init.xavier_normal_(x.weight, gain=1) if type(x) == torch.nn.Conv2d else None)
        self.conv2d_block_5.apply(lambda x: torch.nn.init.xavier_normal_(x.weight, gain=1) if type(x) == torch.nn.Conv2d else None)
        self.fc_block.apply(lambda x: torch.nn.init.xavier_normal_(x.weight, gain=1) if type(x) == torch.nn.Linear else None)
        
        self.loss_function = torch.nn.CrossEntropyLoss()
        
        print ("** ResNet model initialized with hyperparameters: ", hyperparams)
        return

    def forward(self, x_minibatch):
        block_1_outputs = self.conv2d_block_1(x_minibatch)
        # print("** FP block 1 complete! output size: ", block_1_outputs.size())

        block_2_outputs = self.conv2d_block_2(block_1_outputs)
        # print("** FP block 2 complete! output size: ", block_2_outputs.size())
        # block_2_outputs += block_1_outputs
        # block_2_outputs = torch.nn.functional.relu(block_2_outputs)
        # https://stats.stackexchange.com/questions/194142/what-does-1x1-convolution-mean-in-a-neural-network?noredirect=1&lq=1
        # In general, if your input channel and output channel does not match, you can use 1x1 conv to make them match.
        # TODO: these should be conv2d layers ?? is 64,32,1,1 correct ??
        block_2_outputs = self.conv1d_block_2(block_2_outputs) 
        # print("after conv1d block2 out size: ", block_2_outputs.size())
        # .cuda()
        
        block_3_outputs = self.conv2d_block_3(block_2_outputs)
        # print("** FP block 3 complete! output size: ", block_3_outputs.size())
        # if (block_3_outputs.size() != block_2_outputs.size()):
        #     # https://pytorch.org/docs/stable/nn.functional.html#torch.nn.functional.pad
        #     block_3_outputs = torch.nn.functional.pad(block_3_outputs, (8, 8, 8, 8), 'constant', 0)
        #     # print("block 3 output padded size: ", block_3_outputs.size())
        # block_3_outputs += block_2_outputs
        # block_3_outputs = torch.nn.functional.relu(block_3_outputs)
        block_3_outputs = self.conv1d_block_3(block_3_outputs)
        # print("after conv1d block 3 output size: ", block_3_outputs.size())
        # .cuda()

        block_4_outputs = self.conv2d_block_4(block_3_outputs)
        # print("** FP block 4 complete! output size: ", block_4_outputs.size())
        # if (block_4_outputs.size() != block_3_outputs.size()):
        #     # https://pytorch.org/docs/stable/nn.functional.html#torch.nn.functional.pad
        #     block_4_outputs = torch.nn.functional.pad(block_4_outputs, (4, 4, 4, 4), 'constant', 0)
        #     # print("block 4 output padded size: ", block_4_outputs.size())
        # block_4_outputs += block_3_outputs
        # block_4_outputs = torch.nn.functional.relu(block_4_outputs)        
        block_4_outputs = self.conv1d_block_4(block_4_outputs)
        # print("after conv1d block4 out size: ", block_4_outputs.size())
        # .cuda()
        
        block_5_outputs = self.conv2d_block_5(block_4_outputs)
        # print("** FP block 5 complete! output size: ", block_5_outputs.size())        
        # if (block_5_outputs.size() != block_4_outputs.size()):
        #     # https://pytorch.org/docs/stable/nn.functional.html#torch.nn.functional.pad
        #     block_5_outputs = torch.nn.functional.pad(block_5_outputs, (2, 2, 2, 2), 'constant', 0)
        #     # print("block 5 output padded size: ", block_5_outputs.size())
        # block_5_outputs += block_4_outputs
        # block_5_outputs = torch.nn.functional.relu(block_5_outputs)
        # print("after conv1d block5 out size: ", block_5_outputs.size())
        
        (_, C, H, W) = block_5_outputs.data.size()
        block_5_outputs = block_5_outputs.view( -1 , C * H * W)
        outputs = self.fc_block(block_5_outputs)
        return(outputs)
    
    def fit(self, train_loader, test_loader, hyperparams={"learning_rate":0.01, "num_epochs":20}):  
        print ("** Training started with hyperparameters: ", hyperparams)
        time1 = time.time()
        
        # parameters() come from the superclass
        optimizer = torch.optim.SGD(self.parameters(), lr=hyperparams["learning_rate"])
        # optimizer = torch.optim.Adam(self.parameters(), lr=hyperparams["learning_rate"])
        # optimizer = torch.optim.RMSprop(self.parameters(), lr=hyperparams["learning_rate"], momentum=3)
        
        # decay LR by 0.1 every time # of epochs reaches a "milestone" in training 
        scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[25, 40, 60], gamma=0.1)
        
        self.train()
        for epochs in range(hyperparams["num_epochs"]):
            scheduler.step()
            total_correct = 0
            for batch_id, (x_minibatch, y_minibatch) in enumerate(train_loader):

                # send minibatch to GPU
                x_minibatch = x_minibatch.to(device)
                y_minibatch = y_minibatch.to(device)

                optimizer.zero_grad()

                # forward pass
                outputs = self.forward(x_minibatch)
                # print("--------------------------------------")
                loss = self.loss_function(outputs, y_minibatch)
                
                # backward pass
                # TODO: need to manually attach basic blocks to computational graph of backprop??
                loss.backward()

                # needed for bluewaters when using ADAM optimizer
                # if(epochs > 6):
                #     for group in optimizer.param_groups:
                #         for p in group['params']:
                #             state = optimizer.state[p]
                #             if 'step' in state.keys():
                #                 if(state['step']>=1024):
                #                     state['step'] = 1000

                # update parameters
                optimizer.step()
                
                _, predictions = torch.max(outputs.data, 1)
                total_correct += (predictions == y_minibatch).sum().item()
                
            training_accuracy = total_correct/np.float(len(train_loader.dataset))
            print("---- Training Accuracy after Epoch #", epochs+1, " : ", training_accuracy)
            test_acc = self.accuracy(test_loader)
            
            # Additional Info when using cuda
            # if (device.type == 'cuda'):
            #     print('GPU Memory Usage:')
            #     print('Allocated:', torch.cuda.memory_allocated()/(1024**2), 'MB')
            #     print('Cached:   ', torch.cuda.memory_cached()/(1024**2), 'MB')

        time2 = time.time()
        print("** Time taken for Training (minutes): ", (time2-time1)/60.0)
        return

    def accuracy(self, test_loader):
        total_correct = 0
        self.eval()
        with torch.no_grad():
            for batch_id, (x_minibatch, y_minibatch) in enumerate(test_loader):
                
                # send minibatch to GPU
                x_minibatch = x_minibatch.to(device)
                y_minibatch = y_minibatch.to(device)
                
                # forward pass
                outputs = self.forward(x_minibatch)
                
                _, predictions = torch.max(outputs.data, 1)
                total_correct += (predictions == y_minibatch).sum().item()

        test_acc = total_correct/np.float(len(test_loader.dataset))
        print("** Accuracy on Test Set: ", test_acc)
        return (test_acc)


if (__name__ == "__main__"):
    # setup PyTorch to use GPU
    # device = torch.device('cpu')
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print('** Using device:', device)
    print('** Device name: ', torch.cuda.get_device_name(0))

    train_loader, test_loader = load_dataset(batch_size = 512)
    
    model_hyperparameters = {
        "filter_size" : 3
    }
    model = ResNet_model(input_dim = 32, 
                       color_depth = 3,
                       output_dim = 100,
                       hyperparams = model_hyperparameters).to(device)

    # print(model) 

    training_hyperparameters = {
        "learning_rate" : 0.02,
        "num_epochs" : 35
    }
    model.fit(train_loader, test_loader, hyperparams=training_hyperparameters)
    torch.save(model, 'ResNet_model_try.ckpt')
    print("** Trained model saved to disk! **")

    # model = torch.load('DCNN_model_1_retrained.ckpt')
    # print (" ** Trained model loaded from disk! **")
    # # torch.save(model.state_dict(), 'DCNN_model_params.ckpt')
    # # model.load_state_dict(torch.load('DCNN_model.ckpt'))
    
    test_acc = model.accuracy(test_loader)
    print ("** Done! **")