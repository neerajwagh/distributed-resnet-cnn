import torch
import numpy as np
import time
import torchvision
import collections
from torch.autograd import Variable
import torch.distributed as dist
import os
import subprocess
from mpi4py import MPI

 
# https://www.cs.toronto.edu/~kriz/cifar.html
def load_dataset(batch_size=256):
    
    train_transforms = torchvision.transforms.Compose([
        torchvision.transforms.RandomCrop(32, padding=4),
        torchvision.transforms.RandomHorizontalFlip(p=0.2),
        torchvision.transforms.RandomRotation(10),
        torchvision.transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5, hue=0.25),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean=(0.4914, 0.4822, 0.4465), std=(0.2023, 0.1994, 0.2010)),
    ])

    test_transforms = torchvision.transforms.Compose([
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean=(0.4914, 0.4822, 0.4465), std=(0.2023, 0.1994, 0.2010)),
    ])
    
    train_set = torchvision.datasets.CIFAR100(root="./cifar100-torchvision",
                                             train=True,
                                             transform=train_transforms,
                                             download=True)
    train_loader = torch.utils.data.DataLoader(train_set,
                                               batch_size=batch_size,
                                               shuffle=True,
                                               num_workers=0,
                                               pin_memory=True
                                            )

    test_set = torchvision.datasets.CIFAR100(root="./cifar100-torchvision",
                                             train=False,
                                             transform=test_transforms,
                                             download=True)
    test_loader = torch.utils.data.DataLoader(test_set,
                                               batch_size=batch_size,
                                               shuffle=False,
                                               num_workers=0,
                                               pin_memory=True
                                            )
    
    print ("** Train and Test loaders setup with batch size =", batch_size)
    return (train_loader, test_loader)



class BasicBlock(torch.nn.Module):
    def __init__(self, in_channels, out_channels, padding_size=None, kernel_size=3, stride=1):
        super(BasicBlock, self).__init__()
        self.resnet_block = torch.nn.Sequential(collections.OrderedDict([
            ("conv2d", torch.nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=1, bias=False)),
            ("batchnorm", torch.nn.BatchNorm2d(out_channels)),
            ("relu", torch.nn.ReLU()),
            ("conv2d", torch.nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=kernel_size, stride=1, padding=1, bias=False)),
            ("batchnorm", torch.nn.BatchNorm2d(out_channels))
        ]))
        self.padding_size = padding_size
        
    def forward(self, x_minibatch):
        x = self.resnet_block(x_minibatch)
        if (x.size() != x_minibatch.size()):
            x = torch.nn.functional.pad(x, (self.padding_size, self.padding_size, self.padding_size, self.padding_size), 'constant', 0)
        x = x + x_minibatch
        outputs = torch.nn.functional.relu(x)
        return(outputs)

# Generic ResNet model specification
class ResNet_model(torch.nn.Module):

    def __init__(self, input_dim, color_depth, output_dim, rank, num_nodes, hyperparams={"hidden_units":100}):
        # call the __init__() of torch.nn.Module class
        super(ResNet_model, self).__init__()
        self.input_dim = input_dim
        self.color_depth = color_depth
        self.output_dim = output_dim
        self.filter_size = hyperparams["filter_size"]
        self.rank = rank
        self.num_nodes = num_nodes

        # creating containers for each conceptual block of NN layers
        self.conv2d_block_1 = torch.nn.Sequential(collections.OrderedDict([
            ("conv2d", torch.nn.Conv2d(in_channels=self.color_depth, out_channels=32, kernel_size=self.filter_size, stride=1, padding=1, bias=False)),
            ("batchnorm", torch.nn.BatchNorm2d(32)),
            ("relu", torch.nn.ReLU()),
		    ("dropout", torch.nn.Dropout2d(p=0.1))
        ]))
        
        self.conv2d_block_2 = torch.nn.Sequential(collections.OrderedDict([
            ("resnet_block", BasicBlock(in_channels=32, out_channels=32, padding_size=None, kernel_size=self.filter_size, stride=1)),
            ("resnet_block_2", BasicBlock(in_channels=32, out_channels=32, padding_size=None, kernel_size=self.filter_size, stride=1)),
        ]))        
        self.conv1d_block_2 = torch.nn.Conv2d(in_channels=32, out_channels=64, kernel_size=1, bias=False, stride=1, padding=0)
        
        self.conv2d_block_3 = torch.nn.Sequential(collections.OrderedDict([
            ("resnet_block", BasicBlock(in_channels=64, out_channels=64, padding_size=8, kernel_size=self.filter_size, stride=2)),
            ("resnet_block_2", BasicBlock(in_channels=64, out_channels=64, padding_size=8, kernel_size=self.filter_size, stride=1)),
            ("resnet_block_3", BasicBlock(in_channels=64, out_channels=64, padding_size=8, kernel_size=self.filter_size, stride=1)),
            ("resnet_block_4", BasicBlock(in_channels=64, out_channels=64, padding_size=8, kernel_size=self.filter_size, stride=1)),
        ]))
        self.conv1d_block_3 = torch.nn.Conv2d(in_channels=64, out_channels=128, kernel_size=1, bias=False, stride=2, padding=0)

        self.conv2d_block_4 = torch.nn.Sequential(collections.OrderedDict([
            ("resnet_block", BasicBlock(in_channels=128, out_channels=128, padding_size=4, kernel_size=self.filter_size, stride=2)),
            ("resnet_block_2", BasicBlock(in_channels=128, out_channels=128, padding_size=4, kernel_size=self.filter_size, stride=1)),
            ("resnet_block_3", BasicBlock(in_channels=128, out_channels=128, padding_size=4, kernel_size=self.filter_size, stride=1)),
            ("resnet_block_4", BasicBlock(in_channels=128, out_channels=128, padding_size=4, kernel_size=self.filter_size, stride=1)),
        ]))
        self.conv1d_block_4 = torch.nn.Conv2d(in_channels=128, out_channels=256, kernel_size=1, bias=False, stride=2, padding=0)
        
        self.conv2d_block_5 = torch.nn.Sequential(collections.OrderedDict([
            ("resnet_block", BasicBlock(in_channels=256, out_channels=256, padding_size=2, kernel_size=self.filter_size, stride=2)),
            ("resnet_block", BasicBlock(in_channels=256, out_channels=256, padding_size=2, kernel_size=self.filter_size, stride=1)),
            ("max_pool", torch.nn.MaxPool2d(kernel_size=2, stride=2))
        ]))

        self.fc_block = torch.nn.Sequential(collections.OrderedDict([
            ("fc_1", torch.nn.Linear(in_features=4096, out_features=self.output_dim, bias=True)),
        ]))
        
        # self.conv2d_block_1.apply(lambda x: torch.nn.init.xavier_normal(x.weight, gain=3) if type(x) == torch.nn.Conv2d else None)
        # self.conv2d_block_2.apply(lambda x: torch.nn.init.xavier_normal(x.weight, gain=3) if type(x) == torch.nn.Conv2d else None)
        # self.conv2d_block_3.apply(lambda x: torch.nn.init.xavier_normal(x.weight, gain=3) if type(x) == torch.nn.Conv2d else None)
        # self.conv2d_block_4.apply(lambda x: torch.nn.init.xavier_normal(x.weight, gain=3) if type(x) == torch.nn.Conv2d else None)
        # self.conv2d_block_5.apply(lambda x: torch.nn.init.xavier_normal(x.weight, gain=3) if type(x) == torch.nn.Conv2d else None)
        # self.fc_block.apply(lambda x: torch.nn.init.xavier_normal(x.weight, gain=3) if type(x) == torch.nn.Linear else None)
        
        self.loss_function = torch.nn.CrossEntropyLoss()
        
        print ("** ResNet model initialized with hyperparameters: ", hyperparams)
        return

    def forward(self, x_minibatch):
        block_1_outputs = self.conv2d_block_1(x_minibatch)
        
        block_2_outputs = self.conv2d_block_2(block_1_outputs)
        block_2_outputs = self.conv1d_block_2(block_2_outputs)         
        
        block_3_outputs = self.conv2d_block_3(block_2_outputs)
        block_3_outputs = self.conv1d_block_3(block_3_outputs)
        
        block_4_outputs = self.conv2d_block_4(block_3_outputs)
        block_4_outputs = self.conv1d_block_4(block_4_outputs)
        
        block_5_outputs = self.conv2d_block_5(block_4_outputs)
        (_, C, H, W) = block_5_outputs.data.size()
        block_5_outputs = block_5_outputs.view( -1 , C * H * W)
        
        outputs = self.fc_block(block_5_outputs)
        return(outputs)
    
    def fit(self, train_loader, test_loader, hyperparams={"learning_rate":0.01, "num_epochs":20}):  
        print ("** Training started with hyperparameters: ", hyperparams)
        time1 = time.time()
        
        optimizer = torch.optim.SGD(self.parameters(), lr=hyperparams["learning_rate"], momentum=0.9, weight_decay=1e-4)
        
        # decay LR by 0.1 every time # of epochs reaches a "milestone" in training 
        scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[20, 35, 50], gamma=0.1)
        
        # each epoch of training should effectively "see" all of the training data
        for epochs in range(hyperparams["num_epochs"]):
            self.train()
            scheduler.step()
            total_correct = 0
            for batch_id, (x_minibatch, y_minibatch) in enumerate(train_loader):

                # cycle batch_ids among available nodes
                if (batch_id % self.num_nodes != self.rank):
                    continue
                
                # control reaches here only if batch_id % self.num_nodes == self.rank
                
                x_minibatch = Variable(x_minibatch).cuda()
                y_minibatch = Variable(y_minibatch).cuda()

                optimizer.zero_grad()

                # forward pass
                outputs = self.forward(x_minibatch)
                loss = self.loss_function(outputs, y_minibatch)
                
                # backward pass
                loss.backward()

                # take gradients to CPU for communication, sum gradient updates of all nodes and let each node take average on its own
                for param in self.parameters():
                    #print(param.grad.data)
                    tensor0 = param.grad.data.cpu()
                    dist.all_reduce(tensor0, op=dist.reduce_op.SUM)
                    tensor0 /= np.float(self.num_nodes)
                    # send updated synchronized gradients back to GPU
                    param.grad.data = tensor0.cuda()

                # update parameters
                optimizer.step()
                
                _, predictions = torch.max(outputs.data, 1)
                # predictions = 
                total_correct += (predictions == y_minibatch.data).sum()
            
            # for param in self.parameters():
            #     tensor0 = param.data.cpu()
            #     dist.all_reduce(tensor0, op=dist.reduce_op.SUM)
            #     param.data = tensor0 / np.float(self.num_nodes)
            #     param.data = param.data.cuda()

            total_correct = comm.allreduce(total_correct, op=MPI.SUM)
            training_accuracy = total_correct / np.float(len(train_loader.dataset))
            if (self.rank == 0):
                print("---- Training Accuracy after Epoch #", epochs+1, " : ", training_accuracy)
            test_acc = self.accuracy(test_loader)
        
        if (self.rank == 0):    
            time2 = time.time()
            print("** Time taken for Training (minutes): ", (time2-time1)/60.0)
        return  

    def accuracy(self, test_loader):
        test_correct = 0
        self.eval()
        for batch_id, (x_minibatch, y_minibatch) in enumerate(test_loader):
            
            # cycle batch_ids among available nodes
            if (batch_id % self.num_nodes != self.rank):
                continue
            
            # send minibatch to GPU
            x_minibatch = Variable(x_minibatch).cuda()
            y_minibatch = Variable(y_minibatch).cuda()
            
            # forward pass
            outputs = self.forward(x_minibatch)
            
            _, predictions = torch.max(outputs.data, 1)
            test_correct += (predictions == y_minibatch.data).sum()

        test_correct = comm.allreduce(test_correct, op=MPI.SUM)
        test_acc = test_correct/np.float(len(test_loader.dataset))
        if (self.rank == 0):
            print("** Accuracy on Test Set: ", test_acc)
        return (test_acc)


if (__name__ == "__main__"):
    name = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    num_nodes = int(comm.Get_size())

    cmd = "/sbin/ifconfig"
    out, err = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE).communicate()
    ip = str(out).split("inet addr:")[1].split()[0]

    ip = comm.gather(ip)
    if rank != 0:
        ip = None
    ip = comm.bcast(ip, root=0)

    os.environ['MASTER_ADDR'] = ip[0]
    os.environ['MASTER_PORT'] = '2222'

    dist.init_process_group(backend = 'mpi', rank=rank, world_size=num_nodes)
    dtype = torch.FloatTensor

    # test MPI setup of all nodes
    print("Rank : ", rank)
    print('** Device name: ', torch.cuda.get_device_name(0))

    # every node sets up data loaders and model
    train_loader, test_loader = load_dataset(batch_size = 256)
    
    model_hyperparameters = {
        "filter_size" : 3
    }
    model = ResNet_model(input_dim = 32, 
                       color_depth = 3,
                       output_dim = 100,
                       rank = rank,
                       num_nodes = num_nodes,
                       hyperparams = model_hyperparameters)

    # print(model) 
    # model = torch.load('ResNet_model_try.ckpt')
    # print (" ** Trained model loaded from disk! **")

    # ensure all nodes have the same initial model parameters
    for param in model.parameters():
        tensor0 = param.data
        dist.all_reduce(tensor0, op=dist.reduce_op.SUM)
        param.data = tensor0/np.sqrt(np.float(num_nodes))

    # each node moves the model to GPU, once all parameters are guaranteed to be the same across all nodes
    model.cuda()

    training_hyperparameters = {
        "learning_rate" : 0.02,
        "num_epochs" : 40
    }
    # each node starts fitting the model inside GPU
    model.fit(train_loader, test_loader, hyperparams=training_hyperparameters)
    
    # save model and run testing accuracy only on the master node, all other nodes complete at this point
    if (rank == 0):
        torch.save(model, 'ResNet_syncSGD_try2.ckpt')
        print("** Trained model saved to disk! **")
        # # torch.save(model.state_dict(), 'DCNN_model_params.ckpt')
        # # model.load_state_dict(torch.load('DCNN_model.ckpt'))

    # test_acc = model.accuracy(test_loader)
 
    print ("** Done! **")